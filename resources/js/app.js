import Vue from 'vue'
import Vuetify from 'vuetify'
import VueRouter from 'vue-router'

require('./here/mapsjs-core');
require('./here/mapsjs-service');
require('./here/mapsjs-ui');
require('./here/mapsjs-mapevents');

window.axios = require('axios');

window._ = require('lodash');
window.Vue = Vue;
Vue.use(Vuetify);
Vue.use(VueRouter);


import mainMap from './components/main-map';
import profile from './components/profile';
import placeDetail from './components/place-detail';
import placePersonChat from './components/place-person-chat';
import placeChat from './components/place-chat';
import loginForm from './components/login-form';
import chatsHistory from './components/chats-history';
import chatsHistoryDetail from './components/chats-history-detail';

import store from './store'

import VueLocalStorage from 'vue-localstorage'

Vue.use(VueLocalStorage)

const routes = [
  { path: '/map', component: mainMap, name: 'mainmap' },
  { path: '/profile', component: profile, name: 'profile' },
  { path: '/place', component: placeDetail, name: 'place' },
  { path: '/place/message/:id', component: placePersonChat, name: 'place-person-chat' },
  { path: '/placechat', component: placeChat, name: 'place-chat' },
  { path: '/login', component: loginForm, name: 'login-form' },
  { path: '/history', component: chatsHistory, name: 'history' },
  { path: '/history/:id', component: chatsHistoryDetail, name: 'history-detail' },
]

// 3. Создаём экземпляр маршрутизатора и передаём маршруты в опции `routes`
// Вы можете передавать и дополнительные опции, но пока не будем усложнять.

const router = new VueRouter({
  mode: 'history',
  base: '/point/',
  routes // сокращённая запись для `routes: routes`
})

router.beforeResolve((to, from, next) => {
  next();
})

const app = new Vue({
  router,
  store,
  data:function(){
    return {
      drawer: false,
      crf:false,
    }
  },
  mounted:function(){
    store.commit('init');
    setInterval(this.sendPositionData, 5000);
    navigator.geolocation.watchPosition((position)=>{
      this.$store.commit('setposition', position);
    });
  },
  watch:{
    place:function(newval, oldval){
      if(newval){
        axios.get('https://192.168.1.60:8000/userlocations/'+this.place).then((response) => {
          this.$store.commit('setPlaceData', response.data);
        });
      }
    }
  },
  computed:{
    place:function(){
      return this.$store.getters.getPlace;
    },
    placeData:function(){
      return this.$store.getters.getPlaceData;
    },
    currentUser:function(){
      return this.$store.getters.getUser;
    }
  },
  methods:{
    sendPositionData:function(){
      var position = this.$store.getters.getPosition;
      if(position && this.currentUser){
        const params = new URLSearchParams();
        params.append('lng', position.coords.longitude);
        params.append('lat', position.coords.latitude);

        axios.post('https://192.168.1.60:8000/guests/'+this.currentUser.id+'/updateLocation/', params)
          .then( (response)=>{
            if(response.data.in_place){
              this.$store.commit('enterplace', response.data.place_id);
            }else{
              this.$store.commit('exitplace');
            }
          })
      }
    },
    showPlaceChips:function(){
      if(this.place && this.$route.name == 'mainmap'){
        return true;
      }
      return false;
    }
  }
}).$mount('#app')
