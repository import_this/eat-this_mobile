import Vue from 'vue'
import Vuex from 'vuex';
Vue.use(Vuex);
const store = new Vuex.Store({
  state: {
    location: false,
    place: false,
    placeData: {},
    user:null
  },
  mutations: {
    setposition (state, pos) {
      state.location = pos;
    },
    enterplace (state, place) {
      state.place = place;
    },
    exitplace (state) {
      state.place = false;
      state.placeData = {};
    },
    setPlaceData (state, place) {
      state.placeData = place;
    },
    saveUser (state, user) {
      Vue.localStorage.addProperty('saveUser', Object);
      Vue.localStorage.set('saveUser', user, Object);
      state.user = user;
    },
    logOut (state) {
      Vue.localStorage.addProperty('saveUser', Object);
      Vue.localStorage.set('saveUser', null, Object);
      state.user = null;
    },
    init (state){
      var user = Vue.localStorage.get('saveUser', null, Object);
      state.user = user;
      navigator.geolocation.getCurrentPosition(function(position){
        store.commit('setposition', position);
      })
    },
  },
  getters: {
    getPosition: state => {
      return state.location;
    },
    getUser: state => {
      return state.user;
    },
    getPlace: state => {
      return state.place;
    },
    getPlaceData: state => {
      return state.placeData;
    },
    getPlaceUser: (state) => (id) => {
      var find = _.find(state.placeData.guests, (guest)=>{
        return guest.id === id;
      });
      console.log(id);
      return find;
    }
  }
})

export default store;
